# 3. manager sends audit log records

Date: 2024-03-15

## Status

Accepted

## Context

The `Controller` sends records to an Audit log, providing means to audit changes on the configuration* of FSC components within a `Peer`. 
However, it is also possible to directly use the `Manager` to make configuration changes to FSC components. 
Changes done directly via the `Manager` are not sent to the Audit log leaving a possibility of changing the configuration without the visibility and auditability the Audit log provides.

## Decision

According to the [BIO](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/cybersecurity/bio-en-ensia/baseline-informatiebeveiliging-overheid/) and following conversations with architects expressed the need for all components able to modify the configuration to record Audit log records.

## Consequences

To realize this, the `Manager` must also send Audit log records.

*changes of configuration in the scope of FSC and this ADR refer to all `creation`, `modification`, `deletion` of information artifacts in the FSC system. For example: `creation`, `deletion` and/or `modification` of `Services`, `Contracts`, etc.
