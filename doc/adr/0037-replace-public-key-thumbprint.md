# 37. replace public key thumbprint

Date: 2025-03-05

## Status

Declined

## Context

To set up a connection with FSC a Contract with a ServiceConnectionGrant is needed. This Grant contains the public key thumbprint of the Outway that is allowed to connect.
When the certificate of the Outway is renewed and the keypair is rotated, the public key thumbprint will change and new Contract will be required to keep the connection working.
RINIS has raised concerns that this will increase the maintenance burden of FSC. A possible solution could be to replace the public key thumbprint with the Common Name or Subject Alternative Name of the certificate. This will allow organisations to rotate their keypair without needing to create a new Contract.

## Decision

Originally FSC used the certificate thumbprint in the ServiceConnectionGrant which led to feedback from members of the Technisch overleg Digikoppeling (TO). Using the certificate thumbprint means that a Contract **always** needs renewal when a certificate is renewed.
After consulting with the members of the TO it was decided to use the public key thumbprint instead of the certificate thumbprint. A public key thumbprint does not change when a certificate is renewed without changing the keypair.
This allows organisations to make the assessment themselves if they want to renew Contracts. Because this solution was created together with the members of the TO FSC will stick with this solution for now.
RINIS will address this issue during the upcoming TO (19-03-2025) so it can be discussed. 


## Consequences

Contracts containing ServiceConnectionGrants need to be renewed when the keypair of the Outway is rotated.  
