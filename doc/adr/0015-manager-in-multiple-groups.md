# 15. manager in multiple groups

Date: 2023-07-03

## Status

Accepted

## Context
FSC `Managers` are coupled to a single `Group`. Why should a `Manager` not be able to operate in multiple `Groups` at the same time (Open internet, Diginetwerk etc.)? This will reduce the technical overhead.
This proposition was discussed with representatives of the municipalities Den Bosch, Rotterdam and Utrecht. The was understood from a functional perspective but not from a security perspective.

It is undesirable to connect the different networks (FSC `Groups`).
By allowing the `Manager` to operate on multiple `Groups` at the same time you are creating a bridge between networks destroying the essential security feature of a separate network. That it is separate.

## Decision

A FSC `Manager` is only allowed to participate in one FSC `Group`. 

## Consequences

By allowing a `Manager` to only participate in one FSC `Group` the stricter isolation and accompanying security benefits are kept intact.
