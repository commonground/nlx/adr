# 6. unique identifier in certificates

Date: 2023-02-22

## Status

Accepted

## Context
FSC needs a Unique identifier in the certificates used for identifying `Peers` withing the FSC `Group`. FSC used the `subject.Serialnumber` of x.509 Certificates for identifying `Peers`.
It however possible to also use other fiels within the x.509 Certificate. For example the eIDAS standardized `qc statements` used within PSD2. 

## Decision

After feedback from Geonovum and during the `inloop uur` we decided that FSC does not force the usage of the subject.SerialNumber of the certificate as the unique identifier of a Peer.
FSC describes that a `Group` should decide which fields of the subject form the unique identifier of a `Peer`. FSC calls this ID the `Peer ID`. This will make FSC usable in a Public Key Infrastructure which uses certificates that do not implement the `subject serialnumber`.

## Consequences

This allows the flexibility of FSC to handle multiple identifiers as long as they are present in the x.509 Certificate so a FSC `Group` can decide what bests suits their needs.
I.e. FSC will be more usable as a international standard
