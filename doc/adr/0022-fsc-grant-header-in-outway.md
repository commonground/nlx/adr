# 22. Fsc-Grant-Header in Outway

Date: 2023-07-06

## Status

Accepted

## Context
Outways need a way to determine which contract is used in an outgoing connection, so the Outway can use the right `(Delegated) Service Connection Grant` when requesting an `Access Token`. 
Currently, this is done by using the `Fsc-Grant-Header` provided by the client.

The issue motivating this decision, and any context that influences or constrains the decision.

## Decision

The usage of `Fsc-Grant-Header` by the client sending a request to the `Outway` is considered an **implementation** detail. 
The FSC standard will be changed, so it is no longer required by an `Outway` to use the `Fsc-Grant-Header`.

However, the reference implementation, will keep using the `Fsc-Grant-Header` as a means to determine the `(Delegated) Service Connection Grant` to be used when requesting an `Access Token`.
Other parties implementing an FSC `Outway` are free to use other means of determining the `(Delegated) Service Connection Grant` in the Outway.

## Consequences

This makes the standard more flexible and allows other parties implementing an FSC `Outway` to better align this with their internal implementation and architecture.
