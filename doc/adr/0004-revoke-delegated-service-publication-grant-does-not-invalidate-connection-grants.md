# 4. revoke (delegated) service publication grant does not invalidate connection grants

Date: 2024-03-18

## Status

Accepted

## Context
Revoking a `(Delegated) Service Publication grants` does not impact the `(Delegated) Service Connection grants`. 

*Scenario*:
Municipality: `Gemeente Stijns` delegates the publication of a Service to `Saas vendor X` using a `Delegated Service Publication grant`
SaaS vendor: `X` offers a service **on behalf of** `Gemeente Stijns`
Client: `A` connects to the `SaaS Vendor X` to connect to the service

`Gemeente Stijns` revokes the `Delegated Service Publication grant` with `SaaS vendor X`.
The `Service Connection grant` between `Client A` and `SaaS vendor X` remains operational and is not impacted by the revocation of the `Delegated Service Publication Grant`

*Scenario*:
Municipality: `Gemeente Stijns` publishes a Service using a `Service Publication Grant`
Client: `A` connects to `Gemeente Stijns` to connect to the service

`Gemeente Stijns` revokes the `Service Publication grant`.
The `Service Connection grant` between `Client A` and `Gemeente Stijns` remains operational and is not impacted by the revocation of the `Service Publication Grant`

More information is described in the Gitlab [issue](https://gitlab.com/commonground/nlx/fsc-nlx/-/issues/222)

## Decision

The current behaviour described in the [Context](#context) is the *desired* behavior. No changes in the standard will be made to change this behavior

## Consequences

The flexibility the behavior in [Context](#context) allows for the following scenarios:
- `SaaS vendor X` releases a new version of the service. It removes the publication of the old service, so new clients connect to the new service. However, existing connections keep working to allow existing clients a migration period.
- `Municipality Gemeente Stijns` switches from SaaS vendor. It removes the publication of the old service, and clients connect to the new service. However, existing connections keep working to allow existing clients a migration period.
