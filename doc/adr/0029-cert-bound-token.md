# 28. Certificate bound token from cert in connection 

Date: 2024-04-23

## Status

Accepted

## Context

When an Outway wants to connect to a service from a Peer via the Inway, it needs a token to proof he is authenticated and authorized to access that service. This token can be retrieved by the provider Peer via the `GET /token` endpoint. The request body contains the client certificate of the Outway in the `client_id` field. This makes it possible that other components than the Outway can retrieve certificate bound tokens from the provider Peer, because the token gets bound to the certificate in the request body, and not to the certificate of the mutual TLS connection. According to [RFC 8705](https://www.rfc-editor.org/rfc/rfc8705#name-mutual-tls-client-certifica) the token is bound to the certificate of the mutual TLS connection.

## Decision

It was decided that the content of the `client_id` field in the `GET /token` endpoint request body must contain the Peer ID of the connecting Peer. And instead of binding the token to that field, the provider Peer must bind the token to the certificate used by the connecting Peer in the mutual TLS connection. 

## Consequences

This is a breaking change because the content of the `client_id` field in the `GET /token` endpoint request body is changed. This change also forces the connecting Peer to request tokens via components who use the exact same certificate as the Outway certificate. 
