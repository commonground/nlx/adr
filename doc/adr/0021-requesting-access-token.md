# 21. requesting access token

Date: 2023-07-12

## Status

Accepted

## Context
Currently only the `Outway` can request an `Access Token`. 

## Decision
The FSC standard will be changed. The requirement to request an `Access Token` is a client certificate with the same `Peer ID` listed in the `(Delegated) Service Connection Grant`.
In the reference implementation the `Access Token` is requested via the `Manager` of the client.

## Consequences

This will allow greater flexibility in how `Access Tokens` are requested.
