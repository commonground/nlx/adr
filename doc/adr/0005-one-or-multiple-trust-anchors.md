# 5. one or multiple trust anchors

Date: 2023-02-09

## Status

Accepted

## Context

The security corner stone of FSC is the `Trust Anchor` which in practice is the (set of) CA(s) trusted within the FSC `Group`.
FSC specifies there can be only one `Trust Anchor` within a FSC `Group`. But should multiple `Trust Anchors` be supported within a FSC `Group`.

A colleague from Den Bosch wondered why choose to only allowed one trust anchor per Group instead of multiple.

## Decision

We thought about this and concluded that this was because we just always focussed on NLX within the scope of the Dutch governmental organization which all use PKI-o as CA/`Trust Anchor`.
This never raised questions, so we didn't evaluate this restriction.
Now, being questioned about it, we conclude that there's actually no reason for this restriction. We concluded it is up to the `Group` itself to determine which CA's are allowed within the Group.
The only restriction is that it is a finite list of allowed CA's with a `Group`. A `Peer` can use any of the allowed CA's to participate. It's not possible to use a CA which is not in the Groups list.
When using multiple CA's as Trust Anchor the CA's must be able to guarantee the uniqueness of a Peer ID.

Why don't we use the CA list from Mozilla?

it is possible to use this list if you want to
if you use it, you lose control of who can access the network
you must trust the identification process of all CA's in the list depending on how you value the certainty that an organization is who she claims she is.
please note that the CA must enter the Subject Serial Number when issuing certificates
within FSC the Subject Serial number identifies the organization. Hence, unique SSN's are necessary. Using multiple CA's could result in same SSN's for different organizations which will create conflicts between these organizations: from a FSC point of view they are sharing the identity.

## Consequences

This allows flexibility of FSC where `Groups` can determine the `Trust Anchor` tailoring the security needs within the FSC `Group`.
