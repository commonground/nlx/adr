# 14. not mix service connection and publication

Date: 2023-06-14

## Status

Accepted

## Context
A `Contract` can contain multiple `Grants` and also multiple `Grants` of different types. One of these combinations is a `Contract` containing a `(Delegated) Service Publication Grant` and one or more `(Delegated) Service Connection Grants`.

The downside of mixing `Service Publication Grants` with `Service Connection Grants` is that when a `Peer` wants to revoke the `Contract` to cancel the `Service Connection Grant`, the `Service Publication Grant` will also be canceled. 
I.E. the `Service` will be removed from the `Directory`. And of course the converse when a `Peer` decides to `Revoke` the `Service Publication Grant` all the connections to the service in the `Service Connection Grants` are broken.

## Decision

FSC does not allow `Peers` to combine a `(Delegated) Service Publication Grant` with other `Grants`

## Consequences

This does result in needing more `Contracts` for Publishing and Connecting to `Services`. However, this does prevent from **hard coupling** between `Service` Publication and Connectivity.
