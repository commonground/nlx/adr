# 7. delegated service connection grant contains 3 peers

Date: 2023-03-08

## Status

Accepted

## Context

In a `Contract` containing a `Delegated Service Connection` grant three `Peers` are involved:
1. Service provider
2. Delegator
3. Delegatee 

Should all three of these `Peers` be present on the `Contract` and therefore have the ability to `Approve`, `Reject` and `Revoke`?


A argument against the signature of the API Provider is that it should not be required because the decision to delegate the right to connect is a agreement between Delegatee and Delegator and is of no concern to the API Provider.
This is true, but FSC believes the API Provider must be able to exercise control over which Peers are allowed to connect to their Service because the API Provider is responsible for the API.
This is achieved by making the API provider part of the Contract. Another argument would be that the signature requirement will lead to a lot of "work" for the API provider.
We believe this to be untrue because the signature process can easily be automated.
E.g. the API provider can decide to automatically place its signature when a valid Contract with a ServiceConnectionGrant exists between the Delegator and API Provider.

## Decision

Delegation contracts containing a delegated service connection will contain three Peers
When delegating the authorization to connect to a service on behalf of another Peer the API Provider is also required to sign the contract.

## Consequences

The API provider is able to revoke the contract. I.e. the API provider can stop the connection in case the delegatee can no longer be trusted.
The API provider does not have to contact the Delegator to validate a connection attempt. This improves the scalability.
