# 31. explore b2c fsc integration

Date: 2024-06-19

## Status

Proposed

## Context

A taxi driver or a small business could be obligated to add information to a certain datasource (think of the usecases of TNO and SNG). How can we facilitate this B2C data exchange without the necessity of owning an Outway and a Manager.
Suggestion technical solution:
explore idea of mTLS alternative adapters. is it possible to create an extension or adapter to allow alternative methods to mTLS for verified identities of Peers.

## Decision

Multiple options have been explored on a high level, and the one, most promising solution has been detailed a bit further.

These have been documented [here](https://gitlab.com/commonground/nlx/fsc-nlx/-/issues/310).

## Consequences

Put on hold, needs further discussions after better identifying the needs of the market.
