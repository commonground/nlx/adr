# 25. remove peer registration grant

Date: 2024-03-20

## Status

Accepted

## Context

The Peer Registration Grant was used to register a Peer to the Directory. The Grant contained the Peer ID and the adress of the Manager of the Peer.
The Contract containing the Grant was signed by both the Peer trying to register and the Directory.
The purpose of the Grant was not to "validate" the Peer but store the Manager address of the Peer. 
One downside of this approach is that a Peer needs to create a new Contract everytime it's address changes increasing the administrative burden of the system.

## Decision

Since the Grant was never used to validate that a Peer is allowed to operate on a given FSC Group it was decided to remove the Grant.
A `/announce` was added to Manager which allowed Peers to register their own Manager Address.

## Consequences

When the address of a Peer changes, the Peer simply has to "announce" the new address by calling the `/announce` endpoint on the Manager of the Peer acting as Directory. A much simpler process then creating a new Contract, revoking the old one and signing the new one.
