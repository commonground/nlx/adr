# 19. delegation information in token

Date: 2023-04-05

## Status

Accepted

## Context

The `Transaction Log` needs to have information related to the `Delegation` of both `Service Connections` as well as `Service Publications` in order to have full transparency in transactions.
Information in a `Transaction Log` record comes either from the request context or information from the `Access Token` as part of the request context. However, this `Access Token` does not contain any `Delegation` information.
Such as the `Delegator` and/or `Delegatee`. 

## Decision

Add the `Delegation` information as claims in the `Access Token`.
The `Manager` providing the `Access Token` has the required `Delegation` information available in either the `Delegated Service Publication Grant` or the `Delegated Service Connection Grant`. 

## Consequences

The `Transaction Log` will be fully transparent containing `Delegation` information. It will be visible **on behalf** of whom a `Service` is being requested. Or **on behalf** of whom a `Service` is being offered.
