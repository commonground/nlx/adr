# 27. add metadata to transaction log

Date: 2024-03-29

## Status

Proposed

## Context

The old NLX had the possibility to include Headers into the `Transaction Log Records`. 
Some parties use these Headers to include Metadata in a `Transaction Log Record` and are now blocked in migrating to FSC.

## Decision

We would like to extend the transaction log with a metadata field.
This field can contain, in the case of some known users, the headers of the http request.
This will solve the problem with the migration from NLX to the current implementation FSC.

## Consequences

There is a risk parties will misuse these metadata fields, for example storing sensitive information, storing large quantities of information.
Or expect other parties that certain metadata is included in the `Transaction Log` of another `Peer` in the `Group`.

However, it must be noted these risks are implementation risks, not risks of the standard and therefore ultimately the responsibility of the implementor.
