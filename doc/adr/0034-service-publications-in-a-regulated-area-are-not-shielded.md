# 34. service publications in a regulated area are not shielded

Date: 2024-12-24

## Status

Accepted

## Context

A Regulated Area could have directories part of the Regulated Area, also services can be retrieved from the /Services endpoint from Peers directly. There is a difference between Services part of the Regulated Area and Services part of the Group directly. 

## Decision

There will be no differentiation between Services that are part of a Regulated Area and Services that are part of the Group directly. 

## Consequences

Services part of a Regulated Area are visible for Peers that are not part of the Regulated Area. Specifically this means that all Services, including those part of the Regulated Area, are returned in the /Services response.
