# 10. fsc http status codes

Date: 2023-04-12

## Status

Accepted

## Context
FSC uses a custom HTTP status code `540` to indicate FSC specific errors. However, using a custom HTTP status code risks this status code becoming standardized in the future.
In addition, not all libraries and frameworks can be expected to handle custom HTTP status codes.

## Decision

The FSC Error code will be moved to a HTTP response header indicating the presence of a FSC specific error.
The FSC specific HTTP status code will be replaced with standard HTTP status codes.

## Consequences

Using standard HTTP status codes will make sure the status codes remain the same in the future.
