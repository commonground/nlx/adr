# 11. certificate format

Date: 2023-03-12

## Status

Accepted

## Context
The `(Delegated) Service Connection Grants` contain the `Fingerprint` of the `Public key` in the contract. 
Why do you use the PEM format to store the `Public Key Fingerprint`?
PEM files allow new lines, which can result in different hashes. Suggestion would be to store the `Public Key Fingerprint` as bytes.

The issue motivating this decision, and any context that influences or constrains the decision.

## Decision

The description in the standard was wrong. We use the `subjectPublicKeyInfo` field of the x509 certificates which is encoded as DER(binary encoded format). We updated the `Public Key Fingerprint` section of FSC.

## Consequences

Using the DER format of the `Public Key` has the benefit of being compliant to x.509. 
Using the DER format of the `Public key` has the benefit of resulting in stable hashes.
