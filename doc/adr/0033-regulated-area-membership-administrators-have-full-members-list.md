# 33. regulated area membership administrators have full members list

Date: 2024-12-24

## Status

Accepted

## Context

In a Regulated Area it can be possible to have multiple Membership Administrators governing the Regulated Area. This introduces a challenge:
`It is possible that a Peer receiving a Contract is registered by another Membership Administrator than the Peer sending the Contract. How does the Peer Receiving the Contract know where and how to validate the Peer sending the Contract?`

## Decision

In order to avoid a centralized register with possible Membership Administrators and have each Peer retrieve the Members list with all Membership Administrators in the Regulated Area the decision is to synchronize Membership Administrators.
This results in each Membership Administrator have the entire list of Members.

Secondly, the `Role` of each Peer in the Regulated Area will be included in the Members list. Indicating if a Peer is a Member or a Membership Administrator. 
Lastly, for each `Member` entry in the Members list the Membership Administrator who validated this Member is included in the Response.

This does result in Membership Administrators having to synchronize their Members, consequences are listed below. Practical specifics regarding the synchronization like for example the synchronization interval have to be described in the Profile of the Regulated Area.

## Consequences

The complexity is moved from the Peers to the Membership Administrators, since there are fewer Membership Administrators this seems like a reasonable approach. 
However, this complexity does remain and increases by: $`C = \frac{N(N-1)}{2}`$ 

This also introduces a time complexity where the Members list will be eventual consistent. It could therefore be possible the Membership Administrators have not yet fully synchronized and Peers are wrongly omitted from a Members list.
