# 20. retry mechanism

Date: 2023-07-26

## Status

Accepted

## Context
FSC relies on contracts to be **synced** between `Peers` on the contract. 
When failures occur during the synchronization of contracts `Peers` do not have the accurate state of the a contract, or might not have received the contract in the first place.
This could lead to undesired behaviour, for example when a client wants to invoke a `Service` on a contract that has been `Revoked`, but due to synchronization errors this `Revocation` has not synchronized with the client.

## Decision
A `Retry mechanism` will be included in the FSC standard.
The `Retry mechanism` will be added as a *MUST* in the FSC standard. However, details regarding the `Retry mechanism`, for example time periods, retry amounts, etc. will be documented in the `Profile`.

## Consequences

Synchronizing failures are now recoverable withing the boundaries set by the FSC `Group` in the `Profile`. This results that all `Peers` withing the `Group` have the same expectations and behavior when it comes to contract synchronization.
