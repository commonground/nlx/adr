# 35. grants for a Regulated Area contain the name of the Regulated Area

Date: 2024-12-24

## Status

Accepted

## Context

When a Peer creates a Contract with Grants for a Service in a Regulated Area certain information about the Regulated Area is required. As a pure minimum the `membership_administrator_peer_id` is required. 

## Decision

For secondary purposes the Name or identifier of the Regulated Area will also be included in the Grant in the Property: `regulated_area_name`. 

## Consequences

Even though not required for the technical operation of a Regulated Area, the inclusion of the name or identifier of the Regulated Area does increase the clarity for the scope of the Contract. 
It also allows for creating easier overviews and statistics grouping per Regulated Area. For example, how many Services are part of the Regulated Area. How, many connections are part of a Regulated Area. This in turn could make audits easier.
