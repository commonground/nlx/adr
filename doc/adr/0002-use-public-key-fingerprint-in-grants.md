# 2. use public key fingerprint in grants

Date: 2024-03-11

## Status

Accepted

## Context

At this moment the `Service Connection Grant` uses the `Certificate Thumbprint` of the `Outway`.
However, the `Certificate Thumbprint` changes when the certificate is *renewed*. This results in the contract no longer being valid with the new certificate and must be re-created.

## Decision

Using the `Public Key Fingerprint` instead of the `Certificate Thumbprint` results in the ability to renew certificates without changing the `Public Key Fingerprint`.
As long as the same public key is used for the new certificate (which is the best practice when renewing certificates)

The result is the ability to renew Certificates of the `Outway` without invalidating all contracts using this `Outway`.

Since the entire certificate is needed when requesting a Token, see [Consequences](#consequences) the `POST /token` endpoint request is extended with `client_id`.
This `client_id` field will contain the `Outway` certificate in `PEM` format.

## Consequences

Since FSC uses `Certificate Bound Tokens` as described in [RFC8705](https://datatracker.ietf.org/doc/html/rfc8705#name-jwt-certificate-thumbprint-) the Token used in the `Fsc-Authorization` Header needs the `Certificate Thumbprint` and not the `Public Key Thumbprint`.
Requesting tokens can be done directly by the `Outway` or by the `Manager` of the organization of the `Outway`.
Tokens that are requested by a `Manager` cannot provide the `Certificate` of the `Outway` in the TLS connection. In order to provide the `Outway Certificate` to the `Manager` issuing the Tokens the `Outway Certificate` is provided in the `client_id` field in the Token request. 
When validating the `Certificate Thumbprint` before creating the Tokens now requires the `Public Key Thumbprint` and the `Certificate Thumbprint` are required.
The validation also verifies if the Certificate of the `Outway` has the Public Key listed in the contract the entire certificate is needed.
