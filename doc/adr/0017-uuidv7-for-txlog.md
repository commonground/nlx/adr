# 17. UUIDv7 for TxLog

Date: 2023-06-28

## Status

Accepted

## Context

Identifiers for records in the `Transaction Log` need to be globally unique to prevent collisions within an FSC `Group`. Using a UUID for this seems appropriate.
However, storing and retrieving records based on a UUID is not optimal from a database indexing perspective. This results in increased latency for queries and increased resource consumption for the database.

Having a incrementing integer could work but has the following downsides:
- breaks the "globally unique" aspect of a transaction withing the FSC `Group` and although not required this is a desirable property to have.
- leaks additional information to other users in the group, for example when requesting `Transaction Log` records at regular intervals it can be approximated how many transactions have taken place. Which could be sensitive information, especially for SaaS providers.

## Decision

The relatively new [UUIDv7](https://uuid7.com/) standard provides a good balance between uniqueness/entropy and sorting, improving indexing.
The FSC `Transaction Log` records and the `TransactionID` used here will use a UUIDv7.

## Consequences

Provides a globally unique identifier allowing transactions to be uniquely referenced withing the FSC `Group` and also allows for efficient indexing.
