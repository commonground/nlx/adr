# 23. remove PeerRegistrationGrant

Date: 2023-12-11

## Status

Accepted

## Context
Due to the introduction of the `Announce` endpoint the `PeerRegistrationGrant` is no longer used or needed within FSC.

[Issue](https://gitlab.com/commonground/nlx/fsc-nlx/-/issues/131)

## Decision

The `PeerRegistrationGrant` will be removed from both the FSC standard as well as the reference implementation

## Consequences

It is no longer necessary to use the `PeerRegistrationGrant`, this `Grant` has been superseded by the `Announce` endpoint making discovery of `Peer` addresses easier.
