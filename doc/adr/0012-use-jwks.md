# 12. use jwks

Date: 2023-03-12

## Status

Accepted

## Context
Within FSC `Contracts` are signed using JWS (JSON Web Signatures). In order to validate a JWS the `Public Key` of the party that has created the JWS must be used. These `Public Keys` must be available to all parties needing to validate signatures.
`Public Keys` in the context of JWS, also known as JSON Web Key (JWK) can be offered using a JWKS (JSON Web Key Set) endpoint offered by the signer as a means for users to obtain the `Public Key`.

Currently, FSC does not offer a JWKS endpoint.

## Decision

The JWKS endpoint will be added to FSC. We believe it will make adopting FSC in existing gateways easier.
The JWKS endpoint is a commonly used endpoint within the scope of OAuth/OIDC to retrieve keys in order to validate JWTs
The JWKS endpoint will be added to the `Manager`
The JWKS endpoint will be offered on the standard endpoint `/.well-known/jwks.json`

## Consequences

By offering a JWKS endpoint `Public Key` exchange between parties becomes much easier. 
