# 9. support elliptic curve ec cryptography

Date: 2023-04-23

## Status

Accepted

## Context
FSC uses PKI based asymmetric cryptography for signing and verifying `Contracts`. However, currently only `RSA` is supported.
In recent years `Elliptic Curve Cryptography (EC)` has gaining traction as alternative for `RSA` and is also [recommended](https://datatracker.ietf.org/doc/html/rfc8725#name-use-appropriate-algorithms)

## Decision

EC specific algorithms will be included in the FSC standard. With the exception of `Probabilistic Signature Scheme` since this scheme does not have wide support.

## Consequences

Allowing EC based algorithms has the benefit of allowing more modern based algorithms which gained popularity in recent times. It also has a theoretical performance gain.
