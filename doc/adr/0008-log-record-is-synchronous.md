# 8. log record is synchronous

Date: 2023-03-29

## Status

Accepted

## Context

Why is writing records to the `Transaction Log` a synchronous process? This could potentially make FSC slow

## Decision

The intention of the FSC logging extension is that for every request made a `Transaction Log` record exists.
The only way to do this is to ask users to write the `Transaction Log` record before sending the request. If an organisation is worried about performance, FSC states that a system must acknowledge that the `Transaction Log` record is created.
This system could be a in memory system which writes the `Transaction Log` records in bulk to disk.

## Consequences

What happens with `Transaction Log` records in case of a timeout between the `Inway` and `Service`?
The `Transaction Log` record will exist in both the Ouway and Inway. A `Transaction Log` record does not store if a request was successful.
A `Transaction Log` record tells a attempt was made by `Peer` X to call `Service` Y offered by `Peer` Z.
If you want to know if the call was successful you should connect `Transaction Log` of the client and/or API with the `Transaction Log` via the `TransactionID` described in FSC.
