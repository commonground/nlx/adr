# 26. add scheme and port number

Date: 2024-03-21

## Status

Accepted

## Context

Peers announce themselves to the Directory. The announcement call contains the address of the Peer's Manager.

## Decision

The address in the announcement call must contain the scheme (https) and port number (443 or 8443). 

## Consequences

This enables clients to use the address directly without having to make any alterations.
