# 24. add a client ID to the Transactionlog

Date: 2024-03-20

## Status

Rejected

## Context

A stakeholder suggested to include a client ID in the Transaction log. This should be a freeform field that users
can use to link transactions to clients in their infrastructure.

## Decision

Instead of adding a client ID to the Transaction log it was decided to return the Transaction ID to the client which can be stored in the logs of the client.
The reason for this is that the Transaction log should only contain information of which FSC can guarantee the correctness. 
The Transaction log can be used a solid foundation for logging. Each request through FSC generates a unique transaction ID, this ID is returned to both the client and API. 
The client and API should store the transaction ID their respective logs. 

## Consequences

A downside is that users will need to combine multiple sources to create a complete log trail. 
The biggest benefit is that the correctness of the Transaction Log can be guaranteed by FSC. And the format of the logs is predictable allowing a technical inoperable logging system. 
