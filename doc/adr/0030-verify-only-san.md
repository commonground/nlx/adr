# 30. verify only SAN

Date: 2024-07-24

## Status

Accepted

## Context

it seems Go only verifies domains with entries in the SAN. However, SAN is optional, and it is also optional to also include the domain listed in the CN in the SAN. FSC should be able to validate certificates without SAN and validate certificates with the domain in either the CN or the SAN.
Some/Most CA's offer the domain name in both the CN as well as the SAN, however FSC should be able to handle CA issued certs containing only a CN entry.

## Decision

It's decided to not implement this. For the following reasons:

Domain name checking in the CN field is deprecated since the year 2000 rfc2818.
According to the CA-Browser Forum Baseline Requirements chapter 7.1.4.2.1. there must be atleast one subjectAltname entry that contains a DNS name or IP address. This in combination with rfc2818 results in that the CN must always be ignored.

We can always implement this later when someone must have support for a domain name in the CN. But for now it's not needed. Furthermore, the implementation of this requirement is complex because we would have to write our own mTLS handshake function that checks for the subjectAltnames as well as the CN.

## Consequences

If a certificate is issued with only the domain name in the CN golang will throw a validation error. However, this would mean the certificate is issued in a format deprecated for 24 years. It is extremely unlikely certificates have been issued for that validity period.
