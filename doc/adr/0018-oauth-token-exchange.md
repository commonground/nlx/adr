# 18. oauth token exchange

Date: 2023-08-29

## Status

Accepted

## Context
RFC 8693 OAuth 2.0 Token Exchange - feedback out of the kenniscentrum voor API's - beveiligingswerkgroep
This RFC was mentioned during one of the meetings (12th of June 2023) while discussing the potential usage of dynamic client registration in FSC. 
RFC 8693 describes a possible solution for delegation of access rights. We have investigated whether we can use the RFC 'OAuth 2.0 Token Exchange' (RFC 8693) for delegation within FSC.
There are indeed touch points and similarities but also differences and additional complexity compared to FSC.
The touch points are:

FSC also uses a token(endpoint)
FSC also uses JWT for tokens
FSC uses claims to indicate delegation

Differences:
However, there is a fundamental difference, which makes it undesirable to replace the current solution for delegation with the Token Exchange flow. The difference has to do with how an organization proves its identity.
The RFC 8693 talks about a 'subject token' and an 'actor token' which can be exchanged for an access token. The subject token represent the identity of the party on behalf of whom the access token is being requested and the actor token represents the party to whom the access rights are being delegated. The subject and actor tokens act as proof of identity. However, FSC already determines the identity based on the certificate that the organization uses. Therefore, it is unnecessary to use a token to determine the identity.
The identification of an organization based on certificates has been a fundamental choice. We want to take advantage of the governance, trust and possibilities of the PKI-O certificates maintained by the Dutch government.
Instead of a subject token and actor token, FSC uses 'contracts' to determine who acts on behalf of who. The contract has its own creation, signing and validation flows which provide more functionality which are necessary to support the higher goal of the standard which is technical interoperability between all governmental bodies.

Adoption possibilities:
FSC has created claims to indicate that the request is based on delegation. We are considering adopting the claim names used in RFC 8693. This would make the field names more recognizable.

## Decision

FSC does not adopt RFC 8693 because of the differences listed in [Context](#context). It does however align claim names to provide better understanding of the claims.

## Consequences

Aligning on claim names likely increases clarity on FSC claim names regarding delegation.
