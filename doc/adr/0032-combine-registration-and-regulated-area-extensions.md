# 31. combine registration and regulated area extensions

Date: 2024-10-02

## Status

Accepted

## Context

There are two different use cases for both the Registration and the Regulated Area extensions, one coming from Rinis, expects a multitude of Regulated Areas all with various rules and policies for membership and registration.
The other is FDS of which there is only one FDS with one policy, but there can be a layered approach to verifying membership, however in the end all Peers become a member of one FDS/Group.
Since Registration needs to be with an entity, the result of the registration this result must also be defined. This definition needs to align with the Regulated Area.

## Decision

Due to this overlap the functionality of Registration and Regulated Area will be combined in one extension. This also includes the consolidated member list.
Specific rules for validation are deliberately left as open as possible to allow for a wide range of use cases.

## Consequences

The Registration is tied to a Membership administrator that governs the registrations of a Regulated Area. Registrations are always in the context of a Regulated Area.
