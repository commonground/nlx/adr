# 28. add service publication delegator to service connection grants

Date: 2024-04-08

## Status

Accepted

## Context

When connecting to a service that is published using a `delegated service publication` we discovered that since the Delegator information is not present in the (Delegated) Service Connection Grant, it is possible that a malicious or neglectful Delegatee can keep offering a Service on behalf of a Peer delegating the Service Publication. 
Even while the Delegator has revoked the (Delegated) Service Publication Grant.
This can happen when the Delegatee simply ignores the revoke signature from the Delegator.

## Decision

It was decided to include the Delegator of a Service publication in the (Delegated) Service Connection Grant. 
The `service` object in the (Delegated) Service Connection Grant will be extended with a `type` field which indicates if the Service is being offered on behalf of another Peer.
This solution is preferred over adding an optional `delegator` field to the Grant because when a type `delegated service` is used it is possible to validate that the delegator is indeed set.

## Consequences

This is a breaking change because the Grant hashes will change. 
The Delegator of Delegated Service Publication will also have to sign the (Delegated) Service Connection Grants.
The `pdi` (Delegator Peer ID) in the access token can be verified by Service consumer by validating the Contract with the (Delegated) Service Connection Grant.
A Delegator of a Service publication can also revoke all connections to the Service because the Delegator is also part of the Contracts with (Delegated) Service Connection Grants.
