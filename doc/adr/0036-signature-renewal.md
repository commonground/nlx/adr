# 36. signature renewal

Date: 2025-03-05

## Status

Declined

## Context

FSC Contracts are signed with a certificate. Each time a Contract is validated, the signatures are also validated. Signature are created using an X.509 certificate. This certificate needs to be valid. This means that a signature needs to be renewed when the certificate expires before the Contract expires. 
RINIS has raised concerns that this increases the maintenance burden of FSC and suggest FSC should look at possible solutions to validate Contracts without validating the certificate used to sign the Contract.

## Decision

A key feature of FSC is that the Contracts are cryptographically secure meaning that a Contract is the absolute proof that an organisation has agreed to the terms in the Contracts. 
Solutions to decrease the maintenance burden of FSC by, for example, validating the signature only when receiving it and storing that organisation x has approved the Contract thus eliminating the need to verify the signature are detrimental to the security of FSC.
FSC does not want to make concessions regarding security. The solution for decreasing the maintenance burden is automation. The OpenFSC implementation should make it easy to replace signatures that are about to expire.  

## Consequences

OpenFSC needs to be extended with functionality that makes it easy to renew signatures.
Organisations not using OpenFSC need to build functionality that will help renew signatures themselves.
