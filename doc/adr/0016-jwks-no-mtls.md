# 16. JWKS no mTLS

Date: 2023-05-31

## Status

Accepted

## Context

FSC offers a [JWKS endpoint](0012-use-jwks.md) where clients can download the `Public key` to verify signatures on `Access Tokens` and `Contracts`. However, all endpoints in FSC, including the JWKS endpoint use mTLS.
The problem with this is the lack of mTLS support in a lot of frameworks and libraries offering JWKS clients, therefore users cannot use these libraries and frameworks making FSC adoption harder for implementors.

Since the JWKS endpoint in FSC only contains `Public keys` and `Certificates` the additional authentication and security offered by mTLS is not needed.

## Decision

The JWKS endpoint will be offered on a non mTLS endpoint, using only Server Certificates (single sided TLS)

## Consequences

This allows JWKS libraries and clients to use the JWKS endpoint of FSC.
